const express = require('express');

const app = express();

app.use(express.json())
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res, next) => {
  res.send("oke")
})

app.use((req, res, next) => {
  next({ status: 404, message: "not found" })
})

app.use((err, req, res, next) => {
  res.status(err.status || 500).json(err)
})

app.listen(process.env.PORT)